# BioFlow-Insight-Phyloplace-Workflow-case-study

In this repository, the steps to obtain the *Specification graph* and the *Process dependency graph* from the nf-core Phyloplace workflows using **BioFlow-Insight** are described. To run the analysis, execute the following commands:

```
pip install bioflow-insight==1.0
wget https://github.com/nf-core/phyloplace/archive/35c603c935ef50274a895056551b0c4caa2a2468.zip
unzip 35c603c935ef50274a895056551b0c4caa2a2468.zip
rm 35c603c935ef50274a895056551b0c4caa2a2468.zip
bioflow-insight --processes-to-remove "MULTIQC, CUSTOM_DUMPSOFTWAREVERSIONS" phyloplace-35c603c935ef50274a895056551b0c4caa2a2468/main.nf
```

> `unzip` might have to be installed. But it can be replaced by an equivalent tool.

> Note : To install graphviz, in linux you might need to execute this command `sudo apt install graphviz`

The results of the analysis can be found in the `results` folder and the two graphs can be found in the following addresses:

* `results/graphs/specification_graph_wo_labels.png`
* `results/graphs/process_dependency_graph.png`



| <img align="center" src="img/specification_graph_wo_labels.png">  |  <img align="center" src="img/process_dependency_graph.png" > |
|:-:|---|
| Specification Graph without labels |  Process Dependency Graph  |